## assets/music/boards
### test board bgm.wav
Public Domain by Cityfires

Retrieved from [Open Game Art](https://opengameart.org/content/acrostics)

License [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)

### kdevalley.wav
Public Domain by pheonton

Modified by RiderExMachina (David Seward)

Retrieved from [Open Game Art](https://opengameart.org/content/one)

License [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)

## assets/music/menus
### main menu.ogg

Public Domain by Rezoner

Modified by RiderExMachina (David Seward)

Retrieved from [Open Game Art](https://opengameart.org/content/happy-arcade-tune)

License [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)

## assets/music/minigames
### escape from lava.ogg
Public Domain by [cynicmusic] [https://cynicmusic.com(https://cynicmusic.com) [https://pixelsphere.org](https://pixelsphere.org)

Retrieved from [Open Game Art](https://opengameart.org/content/battle-theme-a)

License [CC0](http://creativecommons.org/publicdomain/zero/1.0/)

## assets/sounds/sfx
### victory.ogg

Public Domain by Matthew Pablo [https://matthewpablo.com](https://matthewpablo.com)

Modified by RiderExMachina (David Seward)

Retrieved from [Open Game Art](https://opengameart.org/content/lively-meadow-victory-fanfare-and-song)

License [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)

## assets/sounds/ui
### click1.wav | click2.wav | rollover2.wav

Public Domain by Kenney

Retrieved from [Open Game Art](https://opengameart.org/content/51-ui-sound-effects-buttons-switches-and-clicks)

License [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)

